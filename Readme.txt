
VisualHg 2
Source control plug-in for Visual Studio 2010, 2012 and 2013

-------------------------------------------------------------------------------
Author    Bernd Schrader (2008-2012), Dmitry Popov (2013-2014) and others
Version   2.0.0.10
State     stable
Licence   GNU General Public License (GPL) (v2.0)
-------------------------------------------------------------------------------

Repository URLs:

  https://bitbucket.org/lmn/visualhg2/hg
  https://hg.codeplex.com/forks/lmn/visualhg2

Use Visual Studio 2010, 2012 or 2013 with SDK to build the solution.